package com.gnrgy.ocppserver.service;

import java.util.List;
import java.util.function.Supplier;

import org.jetbrains.annotations.Nullable;

import com.gnrgy.ocppserver.web.dto.request.RFIDRequest;
import com.gnrgy.ocppserver.web.dto.response.RFIDResponse;

import jooq.steve.db.tables.records.OcppTagDetailRecord;
import ocpp.cs._2015._10.IdTagInfo;

public interface CustomOcppTagService {

	public List<RFIDResponse> updateOcppTag(List<RFIDRequest> rfidRequestList);
	
	public OcppTagDetailRecord getTagDetail(String idTag);
	
	@Nullable IdTagInfo getIdTagInfo(@Nullable String idTag, boolean isStartTransactionReqContext,String chargeBoxIdentity);
    @Nullable IdTagInfo getIdTagInfo(@Nullable String idTag, boolean isStartTransactionReqContext,
                                     Supplier<IdTagInfo> supplierWhenException,String chargeBoxIdentity);
}
