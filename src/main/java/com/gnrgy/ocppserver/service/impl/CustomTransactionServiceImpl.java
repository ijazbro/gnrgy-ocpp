package com.gnrgy.ocppserver.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gnrgy.ocppserver.repository.CustomTransactionRepository;
import com.gnrgy.ocppserver.repository.dto.OngoingSessionsDto;
import com.gnrgy.ocppserver.service.CustomTransactionService;

@Service
public class CustomTransactionServiceImpl implements CustomTransactionService{

	@Autowired private CustomTransactionRepository customTransactionRepository;
	
	@Override
	public List<Integer> getActiveTransactionIds(String chargeBoxId, Integer connectorId, String idTag) {
		List<Integer> transactionList = null;
		Integer maxAttempt = 60; // 3 mints
		Integer attempt = 0;
		do {
			try {
				transactionList =  customTransactionRepository.getActiveTransactionIds(chargeBoxId, connectorId,idTag);
				Thread.sleep(3000);
			} catch (Exception e) {
				break;
			}
			attempt++;
		} while (transactionList.isEmpty() && attempt < maxAttempt);
		return transactionList;
	}

	@Override
	public Integer getActiveTransactionIdsByTransactionId(String chargeBoxId, Integer connectorId,
			Integer transactionId) {
		return customTransactionRepository.getActiveTransactionIdsByTransactionId(chargeBoxId, connectorId, transactionId);
	}
	
	@Override
	public List<OngoingSessionsDto> getOngoingSessions(List<String> idTags){
		return customTransactionRepository.getOngoingSessions(idTags);
	}
	
}
