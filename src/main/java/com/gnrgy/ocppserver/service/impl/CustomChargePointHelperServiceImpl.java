package com.gnrgy.ocppserver.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.gnrgy.ocppserver.exception.GnrgyOcppserverException;
import com.gnrgy.ocppserver.repository.CustomChargePointRepository;
import com.gnrgy.ocppserver.repository.dto.ChargeBoxSiteDto;
import com.gnrgy.ocppserver.repository.dto.ConnectorStatusTransactionDto;
import com.gnrgy.ocppserver.service.CustomChargePointHelperService;
import com.gnrgy.ocppserver.web.dto.request.ChargeBoxSiteRequest;
import com.gnrgy.ocppserver.web.dto.response.ChargeBoxSiteResponse;
import com.gnrgy.ocppserver.web.util.Constant;

import de.rwth.idsg.steve.ocpp.CommunicationTask;
import de.rwth.idsg.steve.ocpp.OcppVersion;
import de.rwth.idsg.steve.ocpp.ws.ocpp16.Ocpp16WebSocketEndpoint;
import de.rwth.idsg.steve.repository.TaskStore;
import de.rwth.idsg.steve.repository.dto.ChargePointSelect;
import de.rwth.idsg.steve.repository.dto.ConnectorStatus;
import de.rwth.idsg.steve.service.ChargePointHelperService;
import jooq.steve.db.tables.records.ChargeBoxRecord;
import jooq.steve.db.tables.records.ChargeBoxSiteRecord;
import ocpp.cs._2015._10.RegistrationStatus;

@Service
public class CustomChargePointHelperServiceImpl implements CustomChargePointHelperService {
	
	@Autowired
	protected ChargePointHelperService chargePointHelperService;
	
	@Autowired
	protected CustomChargePointRepository customChargePointRepository;
	
	@Autowired
	private TaskStore taskStore;
	
	@Autowired private Ocpp16WebSocketEndpoint ocpp16WebSocketEndpoint;
	

	public ChargeBoxRecord getChargeBox(String chargeBoxId) {
		return customChargePointRepository.getChargeBox(chargeBoxId);
	}
	
	
	public Boolean isChargeBoxOnline(String chargeBoxId) {
		for(String chargeBox :ocpp16WebSocketEndpoint.getChargeBoxIdList()){
			if(chargeBox.equals(chargeBoxId)) return true;
		}
		return false;
	}
	
	public List<ChargePointSelect> getchargePointSelectList(String chargeBoxId) throws GnrgyOcppserverException {
		List<ChargePointSelect> chargePointSelectList = new ArrayList<>();
		List<RegistrationStatus> inStatusFilter = Arrays.asList(RegistrationStatus.ACCEPTED,
				RegistrationStatus.PENDING);
		for (ChargePointSelect cps : chargePointHelperService.getChargePoints(OcppVersion.V_16, inStatusFilter)) {
			if (cps.getChargeBoxId().equals(chargeBoxId)) {
				chargePointSelectList.add(cps);
				break;
			}
		}
		if (chargePointSelectList.isEmpty()) {
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_CB_OFFLINE,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return chargePointSelectList;
	}

	public CommunicationTask checkTaskFinished(int taskId) {
		CommunicationTask r = null;
		Integer maxAttempt = 60*3; // 3 mints
		Integer attempt = 0;
		do {
			try {
				r = taskStore.get(taskId);
				Thread.sleep(1000);
			} catch (Exception e) {
				break;
			}
			attempt++;
		} while (!r.isFinished() && attempt < maxAttempt);
		return r;
	}
	
	public List<ChargeBoxSiteDto> getChargeBoxSite(List<String> siteIds){
		return customChargePointRepository.getChargeBoxSite(siteIds);
	}
	
	public List<ConnectorStatusTransactionDto> getChargePointConnectorStatus(List<String> chargeBoxList,List<String> connectorTypesList){
		return customChargePointRepository.getChargePointConnectorStatus(chargeBoxList,connectorTypesList);
	}
	
	public List<ChargeBoxSiteResponse> updateChargeBoxSite(List<ChargeBoxSiteRequest> chargeBoxSiteRequestList){
		return customChargePointRepository.updateChargeBoxSite(chargeBoxSiteRequestList);
	}
	
	public ChargeBoxSiteRecord getChargeBoxSite(String chargeBoxId) {
		return customChargePointRepository.getChargeBoxSite(chargeBoxId);
	}
	
	public void addChargeBoxSite(String chargeBoxId) {
		customChargePointRepository.addChargeBoxSite(chargeBoxId);
	}
	
	public void addConnectorDetail(String chargeBoxId,Integer connectorId) {
		customChargePointRepository.addConnectorDetail(chargeBoxId, connectorId);
	}
	
	public void addIpAddress(String chargeBoxId,String ipAddress) {
		customChargePointRepository.addIpAddress(chargeBoxId, ipAddress);
	}
}
