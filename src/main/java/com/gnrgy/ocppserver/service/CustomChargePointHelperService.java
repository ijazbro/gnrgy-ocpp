package com.gnrgy.ocppserver.service;

import java.util.List;

import com.gnrgy.ocppserver.exception.GnrgyOcppserverException;
import com.gnrgy.ocppserver.repository.dto.ChargeBoxSiteDto;
import com.gnrgy.ocppserver.repository.dto.ConnectorStatusTransactionDto;
import com.gnrgy.ocppserver.web.dto.request.ChargeBoxSiteRequest;
import com.gnrgy.ocppserver.web.dto.response.ChargeBoxSiteResponse;

import de.rwth.idsg.steve.ocpp.CommunicationTask;
import de.rwth.idsg.steve.repository.dto.ChargePointSelect;
import jooq.steve.db.tables.records.ChargeBoxRecord;
import jooq.steve.db.tables.records.ChargeBoxSiteRecord;

public interface CustomChargePointHelperService {

	public List<ChargePointSelect> getchargePointSelectList(String chargeBoxId) throws GnrgyOcppserverException;
	
	public CommunicationTask checkTaskFinished(int taskId);
	
	public ChargeBoxRecord getChargeBox(String chargeBoxId);
	
	public List<ChargeBoxSiteDto> getChargeBoxSite(List<String> siteIds);
	
	public List<ConnectorStatusTransactionDto> getChargePointConnectorStatus(List<String> chargeBoxList,List<String> connectorTypesList);
	
	public List<ChargeBoxSiteResponse> updateChargeBoxSite(List<ChargeBoxSiteRequest> chargeBoxSiteRequestList);
	
	public Boolean isChargeBoxOnline(String chargeBoxId);
	
	public ChargeBoxSiteRecord getChargeBoxSite(String chargeBoxId);
	
	public void addChargeBoxSite(String chargeBoxId);
	
	public void addConnectorDetail(String chargeBoxId,Integer connectorId);
	
	public void addIpAddress(String chargeBoxId,String ipAddress);
	
}
