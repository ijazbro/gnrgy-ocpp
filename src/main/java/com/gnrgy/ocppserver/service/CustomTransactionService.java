package com.gnrgy.ocppserver.service;

import java.util.List;

import com.gnrgy.ocppserver.repository.dto.OngoingSessionsDto;

public interface CustomTransactionService {

	public List<Integer> getActiveTransactionIds(String chargeBoxId, Integer connectorId, String idTag);
	
	public Integer getActiveTransactionIdsByTransactionId(String chargeBoxId, Integer connectorId,Integer transactionId);
	
	public List<OngoingSessionsDto> getOngoingSessions(List<String> idTags);
}
