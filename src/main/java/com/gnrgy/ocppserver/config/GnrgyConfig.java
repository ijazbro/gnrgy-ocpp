package com.gnrgy.ocppserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gnrgy.ocppserver.web.filter.AuthenticationInterceptor;
import com.gnrgy.ocppserver.web.filter.PortalAuthenticationInterceptor;

@Configuration
@PropertySource("classpath:main.properties")
public class GnrgyConfig implements WebMvcConfigurer{

	
	@Bean
	public AuthenticationInterceptor authenticationInterceptor() {
		return new AuthenticationInterceptor();
	}
	

	@Bean
	public PortalAuthenticationInterceptor portalAuthenticationInterceptor() {
		return new PortalAuthenticationInterceptor();
	}
	
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(authenticationInterceptor()).addPathPatterns("/mobile/**");
        registry.addInterceptor(portalAuthenticationInterceptor()).addPathPatterns("/portal/**");
    }
    
    @Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
    
    @Bean
    public ObjectMapper objectMapper() {
    	return new ObjectMapper();
    }
}