package com.gnrgy.ocppserver.exception;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class ApiError {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
	Date timestamp; 
	private String errorCode;
	private String debugMessage;
	private List<ApiSubError> subErrors;

	private ApiError() {
		timestamp = new Date();
	}

	public ApiError(String error) {
		this();
		this.errorCode = error;
	}
	
	public ApiError(String error, Throwable ex) {
		this(error);
		this.debugMessage = ex.getLocalizedMessage();
	}
}
