package com.gnrgy.ocppserver.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(value = "/mock")
public class MockController {

	private static final String AUTHENTICATION_MOCK_SUCCESS = "/authentication/success";
	private static final String AUTHENTICATION_MOCK_FAILURE = "/authentication/failure";
	
	@RequestMapping(value = AUTHENTICATION_MOCK_SUCCESS, method = RequestMethod.POST)
	public @ResponseBody Map<String,Boolean> authenticationMockSuccess(HttpServletRequest request) throws Exception {
		try {
			Map<String,Boolean> haspMap=new HashMap<>();
			haspMap.put("Data", true);
			return haspMap;
			} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}
	
	@RequestMapping(value = AUTHENTICATION_MOCK_FAILURE, method = RequestMethod.POST)
	public @ResponseBody Map<String,Boolean> authenticationMockFailure(HttpServletRequest request) throws Exception {
		try {
			Map<String,Boolean> haspMap=new HashMap<>();
			haspMap.put("Data", false);
			return haspMap;
			} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}
	
}
