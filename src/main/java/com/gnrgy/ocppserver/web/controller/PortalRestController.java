package com.gnrgy.ocppserver.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gnrgy.ocppserver.exception.GnrgyOcppserverException;
import com.gnrgy.ocppserver.repository.dto.ChargeBoxSiteDto;
import com.gnrgy.ocppserver.repository.dto.ConnectorStatusTransactionDto;
import com.gnrgy.ocppserver.repository.dto.OngoingSessionsDto;
import com.gnrgy.ocppserver.service.CustomChargePointHelperService;
import com.gnrgy.ocppserver.service.CustomOcppTagService;
import com.gnrgy.ocppserver.service.CustomTransactionService;
import com.gnrgy.ocppserver.web.dto.ConnectorUuid;
import com.gnrgy.ocppserver.web.dto.request.ChangeConfigurationRequest;
import com.gnrgy.ocppserver.web.dto.request.ChargeBoxSiteRequest;
import com.gnrgy.ocppserver.web.dto.request.ChargersByRequest;
import com.gnrgy.ocppserver.web.dto.request.GetConfigurationRequest;
import com.gnrgy.ocppserver.web.dto.request.OngoingSessionsRequest;
import com.gnrgy.ocppserver.web.dto.request.RFIDRequest;
import com.gnrgy.ocppserver.web.dto.response.ChangeConfigurationResponse;
import com.gnrgy.ocppserver.web.dto.response.ChargeBoxSiteResponse;
import com.gnrgy.ocppserver.web.dto.response.ChargerByResponse;
import com.gnrgy.ocppserver.web.dto.response.ConnectorResponse;
import com.gnrgy.ocppserver.web.dto.response.ConnectorTypeResponse;
import com.gnrgy.ocppserver.web.dto.response.GetConfigurationResponse;
import com.gnrgy.ocppserver.web.dto.response.OngoingSessionChargerResponse;
import com.gnrgy.ocppserver.web.dto.response.OngoingSessionConnectorResponse;
import com.gnrgy.ocppserver.web.dto.response.OngoingSessionResponse;
import com.gnrgy.ocppserver.web.dto.response.OngoingSessionsResponse;
import com.gnrgy.ocppserver.web.dto.response.RFIDResponse;
import com.gnrgy.ocppserver.web.util.Constant;

import de.rwth.idsg.steve.ocpp.CommunicationTask;
import de.rwth.idsg.steve.ocpp.RequestResult;
import de.rwth.idsg.steve.ocpp.task.GetConfigurationTask;
import de.rwth.idsg.steve.repository.ChargePointRepository;
import de.rwth.idsg.steve.repository.TaskStore;
import de.rwth.idsg.steve.service.ChargePointHelperService;
import de.rwth.idsg.steve.service.ChargePointService16_Client;
import de.rwth.idsg.steve.web.dto.ocpp.ChangeConfigurationParams;
import de.rwth.idsg.steve.web.dto.ocpp.GetConfigurationParams;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(value = "/portal")
public class PortalRestController {

	private static final String CHARGERS_BY = "/chargers_by";
	private static final String ONGOING_SESSIONS="/ongoing_sessions";
	private static final String UPDATE_CHARGE_BOX_SITE = "/update_charge_box_site";
	private static final String GET_CONFIGURATION= "/get_configuration";
	private static final String CHANGE_CONFIGURATION="/change_configuration";
	private static final String UPDATE_RFID_STATUS="/update_rfid_status";

	@Autowired
	@Qualifier("ChargePointService16_Client")
	private ChargePointService16_Client client16;
	
	@Autowired
	private CustomOcppTagService customOcppTagService;

	@Autowired
	protected ChargePointHelperService chargePointHelperService;
	
	@Autowired private CustomChargePointHelperService customChargePointHelperService;
	
	@Autowired private CustomTransactionService customTransactionService;

	protected ChargePointService16_Client getClient16() {
		return client16;
	}
	
	@PostMapping(value = CHARGERS_BY)
	public @ResponseBody ResponseEntity<?> chargersBy(@RequestBody ChargersByRequest siteRequest, HttpServletRequest request)
			throws Exception {
		try {
			log.info("Request: {} raised with parameters {}", request.getRequestURL(), siteRequest.toString());
			List<ChargerByResponse> chargerByResponseList=new ArrayList<>();
			ChargerByResponse chargersByRequest;
			List<ChargeBoxSiteDto> chargeBoxSiteDtoList = customChargePointHelperService.getChargeBoxSite(siteRequest.getSiteIds());
			
			List<String> chargeBoxIdList=new ArrayList<>();
			for(ChargeBoxSiteDto chargeBoxSiteDto:chargeBoxSiteDtoList) {
				chargeBoxIdList.add(chargeBoxSiteDto.getCharge_box_id());
			}
			
			List<ConnectorStatusTransactionDto> connectorStatusList = customChargePointHelperService.getChargePointConnectorStatus(chargeBoxIdList,siteRequest.getConnectorTypes());
			
			Map<String,List<ConnectorStatusTransactionDto>> connectorStatusMap=new HashMap<>();
			for(ConnectorStatusTransactionDto connectorStaus:connectorStatusList) {
				if(connectorStatusMap.get(connectorStaus.getChargeBoxId()) != null) {
					connectorStatusMap.get(connectorStaus.getChargeBoxId()).add(connectorStaus);
				}else {
					List<ConnectorStatusTransactionDto> newConnectorStatusList = new ArrayList<>();
					newConnectorStatusList.add(connectorStaus);
					connectorStatusMap.put(connectorStaus.getChargeBoxId(),newConnectorStatusList);
				}
			}
			
			
			for(ChargeBoxSiteDto chargeBoxSiteDto:chargeBoxSiteDtoList) {
				if(connectorStatusMap.get(chargeBoxSiteDto.getCharge_box_id())!=null) {
					chargersByRequest=new ChargerByResponse();
					chargersByRequest.setCharge_box_id(chargeBoxSiteDto.getCharge_box_id());
					chargersByRequest.setSite_id(chargeBoxSiteDto.getSite_id());
					chargersByRequest.setId_tag_group_id(chargeBoxSiteDto.getId_tag_group_id());
					chargersByRequest.setCustomer_id(chargeBoxSiteDto.getCustomer_id());
					chargersByRequest.setModel(chargeBoxSiteDto.getModel());
					chargersByRequest.setVendor(chargeBoxSiteDto.getVendor());
					chargersByRequest.setIs_online(customChargePointHelperService.isChargeBoxOnline(chargeBoxSiteDto.getCharge_box_id())?1:0);
					chargersByRequest.setRemote_start(chargeBoxSiteDto.getRemoteStart());
					
					chargersByRequest.setLat(chargeBoxSiteDto.getLattitude());
					chargersByRequest.setLon(chargeBoxSiteDto.getLongitude());
					
					chargersByRequest.setStreet(chargeBoxSiteDto.getStreet());
					chargersByRequest.setCity(chargeBoxSiteDto.getCity());
					chargersByRequest.setCountry(chargeBoxSiteDto.getCountry());
					
					List<ConnectorResponse> connectorResponseList=new ArrayList<>();
						for (ConnectorStatusTransactionDto connectorStatus : connectorStatusMap.get(chargeBoxSiteDto.getCharge_box_id())) {
							String evseId = new StringBuilder().append(chargeBoxSiteDto.getCountry()).append("*").append("GN").append("*")
											.append(("00000000" + chargeBoxSiteDto.getCharge_box_id()).substring(chargeBoxSiteDto.getCharge_box_id().length())).append("*").append(connectorStatus.getConnectorId()).toString();
							ConnectorResponse connectorResponse = new ConnectorResponse(
									ConnectorUuid.getConnectorUuid(connectorStatus.getChargeBoxId(),
											connectorStatus.getConnectorId()),evseId,
									connectorStatus.getLabel(), connectorStatus.getBarCode(), connectorStatus.getStatus(),
									new ConnectorTypeResponse(connectorStatus.getLabel(), connectorStatus.getMaxPower() , connectorStatus.getConnectorTypeStandard(),connectorStatus.getPlugType(),
											connectorStatus.getConnectorChargePointType()), connectorStatus.getTransactionId());
							connectorResponseList.add(connectorResponse);
						}
						chargersByRequest.setConnectors(connectorResponseList);
						chargerByResponseList.add(chargersByRequest);
				}
			}
			log.info("Response: {} ", chargerByResponseList);
			return new ResponseEntity<>(chargerByResponseList, HttpStatus.OK);
		} catch (GnrgyOcppserverException e) {
			throw new GnrgyOcppserverException(e.getMessage(), e.getHttpStatus(), e);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}
	
	@PostMapping(value = UPDATE_CHARGE_BOX_SITE)
	public @ResponseBody ResponseEntity<?> updateChargeBoxSite(@RequestBody List<ChargeBoxSiteRequest> chargeBoxSiteRequestList, HttpServletRequest request)
	throws Exception {
		try {
			log.info("Request: {} raised with parameters {}", request.getRequestURL(), chargeBoxSiteRequestList.toString());
			List<ChargeBoxSiteResponse> chargeBoxSiteResponseList = customChargePointHelperService.updateChargeBoxSite(chargeBoxSiteRequestList);
			log.info("Response: {} ", chargeBoxSiteResponseList);
			return new ResponseEntity<>(customChargePointHelperService.updateChargeBoxSite(chargeBoxSiteRequestList), HttpStatus.OK);
		} catch (GnrgyOcppserverException e) {
			throw new GnrgyOcppserverException(e.getMessage(), e.getHttpStatus(), e);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}
	
	
	
	@PostMapping(value = GET_CONFIGURATION)
	public @ResponseBody ResponseEntity<?>  getConfiguration(@RequestBody GetConfigurationRequest getConfigRequest,HttpServletRequest request) throws Exception {
		try {
			log.info("Request: {} raised with parameters {}", request.getRequestURL(), getConfigRequest.toString());
			GetConfigurationParams getConfigurationParams = new GetConfigurationParams();
			
			getConfigurationParams
					.setChargePointSelectList(customChargePointHelperService.getchargePointSelectList(getConfigRequest.getChargeBoxId()));
			getConfigurationParams.setConfKeyList(getConfigRequest.getKey());
			int taskId = getClient16().getConfiguration(getConfigurationParams);

			CommunicationTask r = customChargePointHelperService.checkTaskFinished(taskId);
			GetConfigurationResponse getConfigurationResponse = populateGetConfiguration(r,getConfigRequest.getChargeBoxId());
			log.info("Response: {} ",getConfigurationResponse);
			return new ResponseEntity<>(getConfigurationResponse,HttpStatus.OK);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}
	
	@PostMapping(value = CHANGE_CONFIGURATION)
	public @ResponseBody ResponseEntity<?> setConfiguration(@RequestBody ChangeConfigurationRequest changeConfigRequest,HttpServletRequest request) throws Exception {
		try {
			log.info("Request: {} raised with parameters {}", request.getRequestURL(), changeConfigRequest.toString());
			ChangeConfigurationParams changeConfigurationParams = new ChangeConfigurationParams();
			
			changeConfigurationParams
					.setChargePointSelectList(customChargePointHelperService.getchargePointSelectList(changeConfigRequest.getChargeBoxId()));
			changeConfigurationParams.setConfKey(changeConfigRequest.getKey());
			changeConfigurationParams.setValue(changeConfigRequest.getValue());
			int taskId = getClient16().changeConfiguration(changeConfigurationParams);

			CommunicationTask r = customChargePointHelperService.checkTaskFinished(taskId);
			ChangeConfigurationResponse changeConfigurationResponse = populateChangeConfiguration(r,changeConfigRequest.getChargeBoxId());
			log.info("Response: {} ",changeConfigurationResponse);
			return new ResponseEntity<>(changeConfigurationResponse,HttpStatus.OK);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}
	
	
	@PostMapping(value = UPDATE_RFID_STATUS)
	public @ResponseBody ResponseEntity<?> updateRFIDStatus(@RequestBody List<RFIDRequest> rfidRequestList,
			HttpServletRequest request) throws Exception {
		try {
			log.info("Request: {} raised with parameters {}", request.getRequestURL(), rfidRequestList.toString());
			List<RFIDResponse> rfidResponseList = customOcppTagService.updateOcppTag(rfidRequestList);
			log.info("Response: {} ",rfidResponseList);
			return new ResponseEntity<>(rfidResponseList,HttpStatus.OK);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}
	
	@PostMapping(value = ONGOING_SESSIONS)
	public @ResponseBody ResponseEntity<?> ongoingSession(@RequestBody OngoingSessionsRequest ongoingSessionsRequest, HttpServletRequest request)
			throws Exception {
		try {
			log.info("Request: {} raised with parameters {}", request.getRequestURL(), ongoingSessionsRequest.toString());
			if(ongoingSessionsRequest.getIdTags() ==null || ongoingSessionsRequest.getIdTags().isEmpty()) {
				log.warn("idTag list empty");
				throw new GnrgyOcppserverException(Constant.ERROR_CODE_ID_TAG_LIST_MISSING,
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			OngoingSessionsResponse ongoingSessionsResponse = new OngoingSessionsResponse();
			ongoingSessionsResponse.setStatus("ok");
			List<OngoingSessionResponse> ongoingSessionList = new ArrayList<>();
			List<OngoingSessionsDto> ongoingSessionsDtoList =  customTransactionService.getOngoingSessions(ongoingSessionsRequest.getIdTags());
			for(OngoingSessionsDto ongoingSessionsDto:ongoingSessionsDtoList) {
				OngoingSessionResponse ongoingSessionResponse=new OngoingSessionResponse();
				ongoingSessionResponse.setTransactionId(ongoingSessionsDto.getTransactionId());
				ongoingSessionResponse.setStartTime(ongoingSessionsDto.getStartTime().toDate());
				ongoingSessionResponse.setIdTag(ongoingSessionsDto.getIdTag());
				
				ongoingSessionResponse.setCharger(new OngoingSessionChargerResponse(ongoingSessionsDto.getUuid(), ongoingSessionsDto.getSiteId(), 
						new OngoingSessionConnectorResponse(ConnectorUuid.getConnectorUuid(ongoingSessionsDto.getUuid(),
								ongoingSessionsDto.getConnectorUuid()), ongoingSessionsDto.getConnectorType(),ongoingSessionsDto.getMaxPower(), ongoingSessionsDto.getConnectorLabel())));
				ongoingSessionList.add(ongoingSessionResponse);
			}
			ongoingSessionsResponse.setOngoingSessions(ongoingSessionList);
			log.info("Response: {} ", ongoingSessionsResponse);
			return new ResponseEntity<>(ongoingSessionsResponse, HttpStatus.OK);
		} catch (GnrgyOcppserverException e) {
			throw new GnrgyOcppserverException(e.getMessage(), e.getHttpStatus(), e);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}

	public GetConfigurationResponse populateGetConfiguration(CommunicationTask r,String chargeBoxId) {
		GetConfigurationResponse response = new GetConfigurationResponse();
		RequestResult reqResult = (RequestResult) r.getResultMap().get(chargeBoxId);
		if (reqResult != null && reqResult.getResponse() != null) {
			if (reqResult.getResponse().equals("OK")) {
				response.setStatus("ok");
				GetConfigurationTask.ResponseWrapper getConfigurationResponse = reqResult.getDetails();
				if(!getConfigurationResponse.getConfigurationKeys().isEmpty()) {
					response.setConfigurationKey(getConfigurationResponse.getConfigurationKeys());
				}
				if(getConfigurationResponse.getUnknownKeys()!=null) {
					response.setUnknownKey(getConfigurationResponse.getUnknownKeys());
				}
			} else {
				response.setStatus("ko");
			}
		} else {
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_NO_RESPONSE_FROM_CB,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	public ChangeConfigurationResponse populateChangeConfiguration(CommunicationTask r,String chargeBoxId) {
		ChangeConfigurationResponse response = new ChangeConfigurationResponse();
		RequestResult reqResult = (RequestResult) r.getResultMap().get(chargeBoxId);
		if (reqResult != null && reqResult.getResponse() != null) {
			response.setStatus(reqResult.getResponse());
		} else {
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_NO_RESPONSE_FROM_CB,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

}
