package com.gnrgy.ocppserver.web.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jooq.tools.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gnrgy.ocppserver.exception.ApiError;
import com.gnrgy.ocppserver.exception.GnrgyOcppserverException;
import com.gnrgy.ocppserver.web.util.Constant;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AuthenticationInterceptor implements HandlerInterceptor {

	@Value("${gnrgy.mobile.authentication.url}")
	String authenticationUrl;

	@Value("${gnrgy.mobile.authentication.enable:false}")
	String authenticationUrlEnable;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ObjectMapper objectMapper;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.debug("Request: {}", request.getRequestURL());
		log.debug("AuthenticationUrlEnable: {}", authenticationUrlEnable);

		if (authenticationUrlEnable.equalsIgnoreCase("true")) {
			String userId = request.getHeader("UserId");
			String accessToken = request.getHeader("AccessToken");

			if (userId == null) {
				throw new GnrgyOcppserverException(Constant.ERROR_CODE_TOKEN_MISSING,HttpStatus.FORBIDDEN);
			}

			if (accessToken == null) {
				throw new GnrgyOcppserverException(Constant.ERROR_CODE_TOKEN_MISSING,HttpStatus.FORBIDDEN);
			}

			JSONObject authenticationRequest = new JSONObject();
			authenticationRequest.put("userId", userId);
			authenticationRequest.put("token", accessToken);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			log.info("Calling gnrgy mobile authentication url: {}", authenticationUrl);
			try {
				String authenticationResponse = restTemplate.postForObject(authenticationUrl, authenticationRequest,
						String.class);
				JsonNode root = objectMapper.readTree(authenticationResponse);
				if (root != null && root.path("Data") != null && root.path("Data").asBoolean()) {
					return true;
				} else {
					throw new GnrgyOcppserverException(Constant.ERROR_CODE_TOKEN_INVALID,HttpStatus.FORBIDDEN);
				}
			} catch (Exception e) {
				throw new GnrgyOcppserverException(Constant.ERROR_CODE_TOKEN_INVALID,HttpStatus.FORBIDDEN);
			}
		}
		return true;
	}
}