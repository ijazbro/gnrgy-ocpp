package com.gnrgy.ocppserver.web.util;

public class Constant {
	
	private Constant() {
		throw new IllegalStateException("Constant class");
	}
	
	public static final String ERROR_CODE_CB_NOT_FOUND="CB_NOT_FOUND";
	public static final String ERROR_CODE_CB_OFFLINE="CB_OFFLINE";
	public static final String ERROR_CODE_TOKEN_INVALID="TOKEN_INVALID";
	public static final String ERROR_CODE_NO_TRANSACTION_STARTED="NO_TRANSACTION_STARTED";
	public static final String ERROR_CODE_TOKEN_MISSING="TOKEN_MISSING";
	public static final String ERROR_CODE_PARAM_MISSING="PARAM_MISSING";
	public static final String ERROR_CODE_PARAM_INVALID="PARAM_INVALID";
	public static final String ERROR_CODE_NOT_FOUND="NOT_FOUND";
	public static final String ERROR_CODE_NO_RESPONSE_FROM_CB="NO_RESPONSE_FROM_CB";
	public static final String ERROR_CODE_IP_ADDRESS_EMPTY="IP_ADDRESS_EMPTY";
	
	public static final String ERROR_CODE_ID_TAG_LIST_MISSING="idTag list is required";
	
	public static final String ERROR_CODE_ID_TAG_MISSING="idTag is required";
	public static final String ERROR_CODE_ID_TAG_INVALID="Invalid";
	public static final String ERROR_CODE_ID_TAG_BLOCKED="Blocked";
	public static final String ERROR_CODE_ID_TAG_EXPIRED="Expired";
	public static final String ERROR_CODE_ID_TAG_CONCURRENTTX="ConcurrentTx";
	public static final String ERROR_CODE_SITE_NOT_FOUND="SITE_NOT_FOUND";
	public static final String ERROR_CODE_SITE_TYPE_NOT_FOUND="SITE_TYPE_NOT_FOUND";
	
	
	
	
}
