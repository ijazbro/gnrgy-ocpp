package com.gnrgy.ocppserver.web.dto.request;

import java.util.List;

import lombok.Data;

@Data
public class ChargersByRequest {

	private List<String> siteIds;
	private List<String> connectorTypes;
	
}
