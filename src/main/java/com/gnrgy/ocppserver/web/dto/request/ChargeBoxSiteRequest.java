package com.gnrgy.ocppserver.web.dto.request;

import lombok.Data;

@Data
public class ChargeBoxSiteRequest {

	String charge_box_id;
	Integer site_id;
	Integer customer_id;
	Integer id_tag_group_id;
	Integer site_type_id;
	Boolean remote_start;
	Boolean authorize_all;
}
