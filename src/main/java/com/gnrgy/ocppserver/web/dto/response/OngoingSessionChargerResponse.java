package com.gnrgy.ocppserver.web.dto.response;

import lombok.Data;

@Data
public class OngoingSessionChargerResponse {

	public OngoingSessionChargerResponse(String uuid, Integer siteId, OngoingSessionConnectorResponse connector) {
		super();
		this.uuid = uuid;
		this.siteId = siteId;
		this.connector = connector;
	}
	String uuid;
	Integer siteId;
	OngoingSessionConnectorResponse connector;
	
}
