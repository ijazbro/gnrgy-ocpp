package com.gnrgy.ocppserver.web.dto.response;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class PingResponse {

	private String status;
	private String latency;
}
