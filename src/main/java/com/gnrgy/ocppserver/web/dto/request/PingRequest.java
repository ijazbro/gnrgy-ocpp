package com.gnrgy.ocppserver.web.dto.request;

import lombok.Data;

@Data
public class PingRequest {

	private String serialNumber; //chargeBoxId
	private String ipAddress;
	
}
