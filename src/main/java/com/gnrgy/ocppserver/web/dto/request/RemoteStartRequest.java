package com.gnrgy.ocppserver.web.dto.request;

import lombok.Data;

@Data
public class RemoteStartRequest {

	private String connector;
	private String identifier;
	
}
