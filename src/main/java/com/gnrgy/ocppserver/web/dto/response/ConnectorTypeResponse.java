package com.gnrgy.ocppserver.web.dto.response;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ConnectorTypeResponse {

	private String label; //connector_detail.label
	private Integer max_power;  //connectorType.max_power(22000)
	private String max_power_string; //connectorType.max_power in kW string (22 kW) 
	private String connector_standard; //connectorType.connectorType
	private String plug_type; //connectorType.shortDescription
	private String charging_facility_type; //connectorType.powerType(AC) + connectorType.max_power(22) AC CHARGING ≤ 22 kW
	private String charging_mode_type; //connectorType.connectorType
	private String charge_point_type; //connectorType.powerType
	
	public ConnectorTypeResponse(String label, BigDecimal max_power,String connector_standard,
			String plug_type, String charge_point_type) {
		super();
		this.label = label;
		if(max_power != null) {
			this.max_power = max_power.multiply(new BigDecimal(1000)).intValue();
			this.max_power_string = String.valueOf(max_power.stripTrailingZeros()) + "kW";	
		}
		this.connector_standard = connector_standard;
		this.plug_type = plug_type;
		
		if(charge_point_type != null) {
			this.charge_point_type = charge_point_type;
			if(this.max_power !=null ) {
				this.charging_facility_type = charge_point_type + " Charging ≤" + this.max_power_string;
			}
		}
		
		this.charging_mode_type = connector_standard;
		
	}
}
