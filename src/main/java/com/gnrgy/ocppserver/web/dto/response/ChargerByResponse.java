package com.gnrgy.ocppserver.web.dto.response;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

@Data
public class ChargerByResponse {

	private String charge_box_id;
	private Integer site_id;
	private Integer customer_id;
	private Integer id_tag_group_id;
	private Integer is_online;
	private Boolean remote_start;
	private String model;
	private String vendor;
	
	private String street;
	private String city;
	private String country;
	
	private BigDecimal lat;
	private BigDecimal lon;
	
	private List<ConnectorResponse> connectors;
}
