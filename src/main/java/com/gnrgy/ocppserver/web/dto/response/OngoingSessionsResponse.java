package com.gnrgy.ocppserver.web.dto.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class OngoingSessionsResponse {

	String status;
	
	@JsonProperty("ongoing_sessions")
	List<OngoingSessionResponse> ongoingSessions;
}
