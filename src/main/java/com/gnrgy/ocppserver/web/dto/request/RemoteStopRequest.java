package com.gnrgy.ocppserver.web.dto.request;

import lombok.Data;

@Data
public class RemoteStopRequest {

	private String connector;
	private Integer transaction;
	private String chargingSessionId;
	
}
