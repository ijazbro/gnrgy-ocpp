package com.gnrgy.ocppserver.web.dto.request;

import java.util.List;
import lombok.Data;

@Data
public class OngoingSessionsRequest {
	
	private List<String> idTags;
	
}
