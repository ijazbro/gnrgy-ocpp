package com.gnrgy.ocppserver.web.dto.request;

import lombok.Data;

@Data
public class ChangeConfigurationRequest {
	
	private String chargeBoxId;
	private String key;
	private String value;
	
}
