package com.gnrgy.ocppserver.web.dto.response;

import lombok.Data;

@Data
public class RemoteStopResponse {

	private String status;
	
}
