package com.gnrgy.ocppserver.web.dto.request;

import java.util.Date;

import lombok.Data;

@Data
public class ReservationRequest {

	private String cp;
	private Integer connector;
	private Date expires;
	private String identifier;
	
}
