package com.gnrgy.ocppserver.web.dto.response;

import com.gnrgy.ocppserver.web.dto.ConnectorUuid;

import lombok.Data;

@Data
public class ConnectorResponse {
	
	private String uuid;
	private String evseid;
	private String label;
	private String barcode;
	private String status;
	private ConnectorTypeResponse type;
	private Integer charging_session;
	
	public ConnectorResponse(String uuid, String evseid, String label, String barcode, String status,
			ConnectorTypeResponse type, Integer charging_session) {
		super();
		this.uuid = uuid;
		this.evseid = evseid;
		this.label = label;
		this.barcode = barcode;
		this.status = status;
		this.type = type;
		this.charging_session = charging_session;
	}
}