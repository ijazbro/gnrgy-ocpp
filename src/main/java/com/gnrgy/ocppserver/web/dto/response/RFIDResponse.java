package com.gnrgy.ocppserver.web.dto.response;

import lombok.Data;

@Data
public class RFIDResponse {

	private String uid;
	private String status;
	
}
