package com.gnrgy.ocppserver.web.dto.response;

import lombok.Data;

@Data
public class ChargeBoxSiteResponse {

	String charge_box_id;
	String status;
	public ChargeBoxSiteResponse(String charge_box_id, String status) {
		super();
		this.charge_box_id = charge_box_id;
		this.status = status;
	}
	
	
}
