package com.gnrgy.ocppserver.web.dto.response;

import lombok.Data;

@Data
public class ReservationResponse {
	
	private String status;
	private Integer reservationId;
}
