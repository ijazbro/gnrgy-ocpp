package com.gnrgy.ocppserver.repository.impl;

import static jooq.steve.db.tables.Address.ADDRESS;
import static jooq.steve.db.tables.ChargeBox.CHARGE_BOX;
import static jooq.steve.db.tables.ChargeBoxSite.CHARGE_BOX_SITE;
import static jooq.steve.db.tables.Connector.CONNECTOR;
import static jooq.steve.db.tables.ConnectorStatus.CONNECTOR_STATUS;
import static jooq.steve.db.tables.ConnectorTypes.CONNECTOR_TYPES;
import static jooq.steve.db.tables.ConnectorsDetail.CONNECTORS_DETAIL;
import static jooq.steve.db.tables.Transaction.TRANSACTION;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gnrgy.ocppserver.repository.CustomChargePointRepository;
import com.gnrgy.ocppserver.repository.dto.ChargeBoxSiteDto;
import com.gnrgy.ocppserver.repository.dto.ConnectorStatusTransactionDto;
import com.gnrgy.ocppserver.web.dto.request.ChargeBoxSiteRequest;
import com.gnrgy.ocppserver.web.dto.response.ChargeBoxSiteResponse;

import de.rwth.idsg.steve.utils.DateTimeUtils;
import jooq.steve.db.tables.records.ChargeBoxRecord;
import jooq.steve.db.tables.records.ChargeBoxSiteRecord;

@Repository
public class CustomChargePointRepositoryImpl implements CustomChargePointRepository{

	private final DSLContext ctx;
	
	@Autowired
    public CustomChargePointRepositoryImpl(DSLContext ctx) {
        this.ctx = ctx;
    }
	
	@Override
	public ChargeBoxRecord getChargeBox(String chargeBoxId) {
		return ctx.selectFrom(CHARGE_BOX)
                .where(CHARGE_BOX.CHARGE_BOX_ID.equal(chargeBoxId))
                .fetchOne();
	}
	
	@Override
	public ChargeBoxSiteRecord getChargeBoxSite(String chargeBoxId) {
		return ctx.selectFrom(CHARGE_BOX_SITE)
                .where(CHARGE_BOX_SITE.CHARGE_BOX_ID.equal(chargeBoxId))
                .fetchOne();
	}

	public List<ChargeBoxSiteDto> getChargeBoxSite(List<String> siteIds) {
		return ctx.select(CHARGE_BOX_SITE.CHARGE_BOX_ID,
				CHARGE_BOX_SITE.SITE_ID,
				CHARGE_BOX_SITE.CUSTOMER_ID,
				CHARGE_BOX_SITE.TAG_GROUP_ID,
				CHARGE_BOX.LOCATION_LATITUDE,
				CHARGE_BOX.LOCATION_LONGITUDE,
				ADDRESS.STREET,
				ADDRESS.CITY,
				ADDRESS.COUNTRY,
				CHARGE_BOX_SITE.REMOTE_START,
				CHARGE_BOX.CHARGE_POINT_MODEL,
				CHARGE_BOX.CHARGE_POINT_VENDOR
				)
		.from(CHARGE_BOX_SITE)
		.join(CHARGE_BOX)
        .on(CHARGE_BOX.CHARGE_BOX_ID.eq(CHARGE_BOX_SITE.CHARGE_BOX_ID))
        .leftOuterJoin(ADDRESS)
        .on(CHARGE_BOX.ADDRESS_PK.eq(ADDRESS.ADDRESS_PK))
        .where(CHARGE_BOX_SITE.SITE_ID.in(siteIds))
        .fetch()
        .map(r -> ChargeBoxSiteDto.builder()
        		.charge_box_id(r.value1())
        		.site_id(r.value2())
        		.customer_id(r.value3())
        		.id_tag_group_id(r.value4())
        		.lattitude(r.value5())
        		.longitude(r.value6())
        		.street(r.value7())
        		.city(r.value8())
        		.country(r.value9())
        		.remoteStart(r.value10())
        		.model(r.value11())
        		.vendor(r.value12())
        		.build()
        		);
	}
	
	@Override
    public List<ConnectorStatusTransactionDto> getChargePointConnectorStatus(List<String> chargeBoxList,List<String> connectorTypesList) {
        // find out the latest timestamp for each connector
        Field<Integer> t1Pk = CONNECTOR_STATUS.CONNECTOR_PK.as("t1_pk");
        Field<DateTime> t1TsMax = DSL.max(CONNECTOR_STATUS.STATUS_TIMESTAMP).as("t1_ts_max");
        Table<?> t1 = ctx.select(t1Pk, t1TsMax)
                         .from(CONNECTOR_STATUS)
                         .groupBy(CONNECTOR_STATUS.CONNECTOR_PK)
                         .asTable("t1");

        // get the status table with latest timestamps only
        Field<Integer> t2Pk = CONNECTOR_STATUS.CONNECTOR_PK.as("t2_pk");
        Field<DateTime> t2Ts = CONNECTOR_STATUS.STATUS_TIMESTAMP.as("t2_ts");
        Field<String> t2Status = CONNECTOR_STATUS.STATUS.as("t2_status");
        Field<String> t2Error = CONNECTOR_STATUS.ERROR_CODE.as("t2_error");
        Table<?> t2 = ctx.selectDistinct(t2Pk, t2Ts, t2Status, t2Error)
                         .from(CONNECTOR_STATUS)
                         .join(t1)
                            .on(CONNECTOR_STATUS.CONNECTOR_PK.equal(t1.field(t1Pk)))
                            .and(CONNECTOR_STATUS.STATUS_TIMESTAMP.equal(t1.field(t1TsMax)))
                         .asTable("t2");

        final Condition chargeBoxCondition = CHARGE_BOX.CHARGE_BOX_ID.in(chargeBoxList);
        final Condition connectorTypeCondition;
        if (connectorTypesList == null || connectorTypesList.isEmpty()) {
        	connectorTypeCondition = DSL.noCondition();
        } else {
        	connectorTypeCondition = CONNECTOR_TYPES.CONNECTOR_TYPE.in(connectorTypesList);
        }
        
        
        //Fetch max transaction Id
        Field<Integer> connectorPk = TRANSACTION.CONNECTOR_PK.as("connectorPk");
        Field<Integer> transactionPkMax = DSL.max(TRANSACTION.TRANSACTION_PK).as("transactionPkMax");
        Table<?> transactionMax = ctx.select(connectorPk, transactionPkMax)
                         .from(TRANSACTION)
                         .where(TRANSACTION.STOP_TIMESTAMP.isNull())
                         .groupBy(TRANSACTION.CONNECTOR_PK)
                         .asTable("transactionMax");
        
        return ctx.select(
                        CHARGE_BOX.CHARGE_BOX_PK,
                        CONNECTOR.CHARGE_BOX_ID,
                        CONNECTOR.CONNECTOR_ID,
                        t2.field(t2Ts),
                        t2.field(t2Status),
                        t2.field(t2Error),
                transactionMax.field(transactionPkMax),
        		CONNECTORS_DETAIL.LABEL,
        		CONNECTORS_DETAIL.BARCODE,
        		CONNECTORS_DETAIL.MAX_POWER,
        		CONNECTOR_TYPES.CONNECTOR_TYPE,
        		CONNECTOR_TYPES.SHORT_DESCRIPTION,
        		CONNECTOR_TYPES.POWER_TYPE
        		)
                  .from(CONNECTOR)
                  .leftOuterJoin(t2)
                        .on(CONNECTOR.CONNECTOR_PK.eq(t2.field(t2Pk)))
                  .leftOuterJoin(transactionMax)
                  		.on(CONNECTOR.CONNECTOR_PK.eq(transactionMax.field(connectorPk)))
                  .join(CHARGE_BOX)
                        .on(CHARGE_BOX.CHARGE_BOX_ID.eq(CONNECTOR.CHARGE_BOX_ID))
                  .join(CONNECTORS_DETAIL)
                  		.on(CONNECTOR.CONNECTOR_ID.eq(CONNECTORS_DETAIL.CONNECTOR_ID))
                  		.and(CONNECTORS_DETAIL.CHARGE_BOX_ID.eq(CONNECTOR.CHARGE_BOX_ID))
                  .leftOuterJoin(CONNECTOR_TYPES)
                  		.on(CONNECTORS_DETAIL.CONNECTOR_TYPE_ID.eq(CONNECTOR_TYPES.ID))
                  .where(chargeBoxCondition,connectorTypeCondition)
                  .orderBy(t2.field(t2Ts).desc())
                  .fetch()
                  .map(r -> ConnectorStatusTransactionDto.builder()
                                           .chargeBoxPk(r.value1())
                                           .chargeBoxId(r.value2())
                                           .connectorId(r.value3())
                                           .timeStamp(DateTimeUtils.humanize(r.value4()))
                                           .statusTimestamp(r.value4())
                                           .status(r.value5())
                                           .errorCode(r.value6())
                                           .transactionId(r.value7())
                                           .label(r.value8())
                                           .barCode(r.value9())
                                           .maxPower(r.value10())
                                           .connectorTypeStandard(r.value11())
                                           .plugType(r.value12())
                                           .connectorChargePointType(r.value13())
                                           .build()
                  );
    }
	
	public List<ChargeBoxSiteResponse> updateChargeBoxSite(List<ChargeBoxSiteRequest> chargeBoxSiteRequestList) {
		List<ChargeBoxSiteResponse> chargeBoxSiteResponseList= new ArrayList<>();
		for(ChargeBoxSiteRequest chargeBoxSiteRequest:chargeBoxSiteRequestList) {
			try {
				if(getChargeBoxSite(chargeBoxSiteRequest.getCharge_box_id()) != null){
					ctx.update(CHARGE_BOX_SITE)
					   .set(CHARGE_BOX_SITE.SITE_ID,chargeBoxSiteRequest.getSite_id())
					   .set(CHARGE_BOX_SITE.TAG_GROUP_ID,chargeBoxSiteRequest.getId_tag_group_id())
					   .set(CHARGE_BOX_SITE.CUSTOMER_ID,chargeBoxSiteRequest.getCustomer_id())
					   .set(CHARGE_BOX_SITE.SITE_TYPE_ID, chargeBoxSiteRequest.getSite_type_id())
					   .set(CHARGE_BOX_SITE.REMOTE_START, chargeBoxSiteRequest.getRemote_start())
					   .set(CHARGE_BOX_SITE.AUTHORIZE_ALL, chargeBoxSiteRequest.getAuthorize_all())
					   .where(CHARGE_BOX_SITE.CHARGE_BOX_ID.equal(chargeBoxSiteRequest.getCharge_box_id()))
					   .execute();
				}else {
					 ctx.insertInto(CHARGE_BOX_SITE)
					 		.set(CHARGE_BOX_SITE.CHARGE_BOX_ID,chargeBoxSiteRequest.getCharge_box_id())
							.set(CHARGE_BOX_SITE.SITE_ID,chargeBoxSiteRequest.getSite_id())
							.set(CHARGE_BOX_SITE.TAG_GROUP_ID,chargeBoxSiteRequest.getId_tag_group_id())
							.set(CHARGE_BOX_SITE.CUSTOMER_ID,chargeBoxSiteRequest.getCustomer_id())
							.set(CHARGE_BOX_SITE.SITE_TYPE_ID, chargeBoxSiteRequest.getSite_type_id())
							.set(CHARGE_BOX_SITE.REMOTE_START, chargeBoxSiteRequest.getRemote_start())
							.set(CHARGE_BOX_SITE.AUTHORIZE_ALL, chargeBoxSiteRequest.getAuthorize_all())
							.execute();
				}
				chargeBoxSiteResponseList.add(new ChargeBoxSiteResponse(chargeBoxSiteRequest.getCharge_box_id(),"Updated"));
			}catch(DataAccessException e) {
				chargeBoxSiteResponseList.add(new ChargeBoxSiteResponse(chargeBoxSiteRequest.getCharge_box_id(),"Error"));
			}
		}
		return chargeBoxSiteResponseList;
	}
	
	public void addChargeBoxSite(String chargeBoxId) {
		if (getChargeBoxSite(chargeBoxId) == null) {
			ctx.insertInto(CHARGE_BOX_SITE).set(CHARGE_BOX_SITE.CHARGE_BOX_ID, chargeBoxId).execute();
		}
	}
	
	public void addConnectorDetail(String chargeBoxId,Integer connectorId) {
		ctx.insertInto(CONNECTORS_DETAIL,
				CONNECTORS_DETAIL.CHARGE_BOX_ID, CONNECTORS_DETAIL.CONNECTOR_ID)
           .values(chargeBoxId, connectorId)
           .onDuplicateKeyIgnore() // Important detail
           .execute();
	}
	
	public void addIpAddress(String chargeBoxId,String ipAddress) {
		ctx.update(CHARGE_BOX)
		   .set(CHARGE_BOX.IP_ADDRESS,ipAddress)
		   .where(CHARGE_BOX.CHARGE_BOX_ID.equal(chargeBoxId))
		   .execute();
	}
	
}
