package com.gnrgy.ocppserver.repository.impl;

import static jooq.steve.db.tables.OcppTag.OCPP_TAG;
import static jooq.steve.db.tables.OcppTagDetail.OCPP_TAG_DETAIL;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gnrgy.ocppserver.repository.CustomOcppTagRepository;
import com.gnrgy.ocppserver.web.dto.request.RFIDRequest;
import com.gnrgy.ocppserver.web.dto.response.RFIDResponse;
import com.google.common.base.Strings;

import jooq.steve.db.tables.records.OcppTagDetailRecord;
import jooq.steve.db.tables.records.OcppTagRecord;

@Repository
public class CustomOcppTagRepositoryImpl implements CustomOcppTagRepository {

	private final DSLContext ctx;

	@Autowired
	public CustomOcppTagRepositoryImpl(DSLContext ctx) {
		this.ctx = ctx;
	}

	public List<RFIDResponse> updateOcppTag(List<RFIDRequest> rfidRequestList) {
		List<RFIDResponse> rfidResponseList = new ArrayList<>();

		for (RFIDRequest rfidRequest : rfidRequestList) {
			RFIDResponse rfidResponse = new RFIDResponse();
			rfidResponse.setUid(rfidRequest.getUid());
			try {
				if (getTag(rfidRequest.getUid()) != null) {
					ctx.update(OCPP_TAG).set(OCPP_TAG.PARENT_ID_TAG, Strings.isNullOrEmpty(rfidRequest.getParentId())?null:rfidRequest.getParentId())
							.set(OCPP_TAG.EXPIRY_DATE, rfidRequest.getExpiresAt()!=null ? new DateTime(rfidRequest.getExpiresAt()):null)
							.set(OCPP_TAG.MAX_ACTIVE_TRANSACTION_COUNT, rfidRequest.getMaxActiveTransactionCount())
							.where(OCPP_TAG.ID_TAG.eq(rfidRequest.getUid())).execute();
				} else {
					ctx.insertInto(OCPP_TAG).set(OCPP_TAG.ID_TAG, rfidRequest.getUid())
							.set(OCPP_TAG.PARENT_ID_TAG, Strings.isNullOrEmpty(rfidRequest.getParentId())?null:rfidRequest.getParentId())
							.set(OCPP_TAG.EXPIRY_DATE, rfidRequest.getExpiresAt()!=null ? new DateTime(rfidRequest.getExpiresAt()):null)
							.set(OCPP_TAG.MAX_ACTIVE_TRANSACTION_COUNT, rfidRequest.getMaxActiveTransactionCount())
							.execute();
				}

				if (getTagDetail(rfidRequest.getUid()) != null) {
					/*
					Record record = ctx.newRecord(OCPP_TAG_DETAIL);
					record.set(OCPP_TAG_DETAIL.ALLOW_PUBLIC_CHARGING, rfidRequest.getAllowPublicCharging());
					record.set(OCPP_TAG_DETAIL.IS_ACTIVE, rfidRequest.getIsActive());
					record.set(OCPP_TAG_DETAIL.LABEL, rfidRequest.getLabel()!=null?rfidRequest.getLabel():null);
					if(rfidRequest.getIdTagGroup()!=null) {
						record.set(OCPP_TAG_DETAIL.TAG_GROUP_ID,rfidRequest.getIdTagGroup());
					}else {
						record.set
					}
					record.set(OCPP_TAG_DETAIL.TAG_GROUP_ID, rfidRequest.getIdTagGroup()!=null?rfidRequest.getIdTagGroup():null);
					record.set(OCPP_TAG_DETAIL.CUSTOMER_ID, rfidRequest.getCustId()!=null?rfidRequest.getCustId():null);
					*/
					
					ctx.update(OCPP_TAG_DETAIL)
							.set(OCPP_TAG_DETAIL.ALLOW_PUBLIC_CHARGING, rfidRequest.getAllowPublicCharging())
							.set(OCPP_TAG_DETAIL.IS_ACTIVE, rfidRequest.getIsActive())
							.set(OCPP_TAG_DETAIL.LABEL, rfidRequest.getLabel()!=null?rfidRequest.getLabel():null)
							.set(OCPP_TAG_DETAIL.TAG_GROUP_ID, rfidRequest.getIdTagGroup()!=null?rfidRequest.getIdTagGroup():null)
							.set(OCPP_TAG_DETAIL.CUSTOMER_ID, rfidRequest.getCustId()!=null?rfidRequest.getCustId():null)
							.where(OCPP_TAG_DETAIL.OCPP_TAG_ID.eq(rfidRequest.getUid())).execute();
				} else {
					ctx.insertInto(OCPP_TAG_DETAIL).set(OCPP_TAG_DETAIL.OCPP_TAG_ID, rfidRequest.getUid())
							.set(OCPP_TAG_DETAIL.ALLOW_PUBLIC_CHARGING, rfidRequest.getAllowPublicCharging())
							.set(OCPP_TAG_DETAIL.IS_ACTIVE, rfidRequest.getIsActive())
							.set(OCPP_TAG_DETAIL.LABEL, rfidRequest.getLabel()!=null?rfidRequest.getLabel():null)
							.set(OCPP_TAG_DETAIL.TAG_GROUP_ID, rfidRequest.getIdTagGroup()!=null?rfidRequest.getIdTagGroup():null)
							.set(OCPP_TAG_DETAIL.CUSTOMER_ID, rfidRequest.getCustId()!=null?rfidRequest.getCustId():null).execute();
				}
				rfidResponse.setStatus("Updated");
			} catch (DataAccessException e) {
				rfidResponse.setStatus("Error");
			}
			rfidResponseList.add(rfidResponse);
		}
		return rfidResponseList;
	}

	@Override
	public OcppTagRecord getTag(String idTag) {
		return ctx.selectFrom(OCPP_TAG).where(OCPP_TAG.ID_TAG.equal(idTag)).fetchOne();
	}

	@Override
	public OcppTagDetailRecord getTagDetail(String idTag) {
		return ctx.selectFrom(OCPP_TAG_DETAIL).where(OCPP_TAG_DETAIL.OCPP_TAG_ID.equal(idTag)).fetchOne();
	}

}
