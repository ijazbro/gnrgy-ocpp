package com.gnrgy.ocppserver.repository.impl;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gnrgy.ocppserver.repository.SiteRepository;

@Repository
public class SiteRepositoryImpl implements SiteRepository{

	private final DSLContext ctx;
	
	@Autowired
    public SiteRepositoryImpl(DSLContext ctx) {
        this.ctx = ctx;
    }

}
