package com.gnrgy.ocppserver.repository.dto;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OngoingSessionsDto {

	Integer transactionId;
	DateTime startTime;
	String idTag;
	String uuid;
	Integer siteId;
	Integer connectorUuid;
	String connectorType;
	String connectorPower;
	BigDecimal maxPower;
	String connectorLabel;
}
