package com.gnrgy.ocppserver.repository;

import java.util.List;

import com.gnrgy.ocppserver.web.dto.request.RFIDRequest;
import com.gnrgy.ocppserver.web.dto.response.RFIDResponse;

import jooq.steve.db.tables.OcppTag;
import jooq.steve.db.tables.records.OcppTagDetailRecord;
import jooq.steve.db.tables.records.OcppTagRecord;

public interface CustomOcppTagRepository {

	public List<RFIDResponse> updateOcppTag(List<RFIDRequest> rfidRequestList);
	
	public OcppTagRecord getTag(String idTag);
	
	public OcppTagDetailRecord getTagDetail(String idTag);
}
