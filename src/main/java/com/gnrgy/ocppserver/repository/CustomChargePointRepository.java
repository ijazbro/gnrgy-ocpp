package com.gnrgy.ocppserver.repository;

import java.util.List;

import com.gnrgy.ocppserver.repository.dto.ChargeBoxSiteDto;
import com.gnrgy.ocppserver.repository.dto.ConnectorStatusTransactionDto;
import com.gnrgy.ocppserver.web.dto.request.ChargeBoxSiteRequest;
import com.gnrgy.ocppserver.web.dto.response.ChargeBoxSiteResponse;

import jooq.steve.db.tables.records.ChargeBoxRecord;
import jooq.steve.db.tables.records.ChargeBoxSiteRecord;

public interface CustomChargePointRepository {

	public ChargeBoxRecord getChargeBox(String chargeBoxId);
	
	public ChargeBoxSiteRecord getChargeBoxSite(String chargeBoxId);
	
	public List<ChargeBoxSiteDto> getChargeBoxSite(List<String> siteIds);
	
	public List<ConnectorStatusTransactionDto> getChargePointConnectorStatus(List<String> chargeBoxList,List<String> connectorTypesList);
	
	public List<ChargeBoxSiteResponse> updateChargeBoxSite(List<ChargeBoxSiteRequest> chargeBoxSiteRequestList);
	
	public void addChargeBoxSite(String chargeBoxId);
	
	public void addConnectorDetail(String chargeBoxId,Integer connectorId);
	
	public void addIpAddress(String chargeBoxId,String ipAddress);
	
}
