ALTER TABLE `ocpp_tag_detail`
    DROP COLUMN `allow_concurrent_tx`;

ALTER TABLE `charge_box`
    DROP COLUMN `remote_start`;

ALTER TABLE `charge_box_site`
    MODIFY COLUMN `site_id` int(11);

ALTER TABLE `charge_box_site`
    ADD COLUMN `remote_start` TINYINT(1) AFTER `site_type_id`;
    
ALTER TABLE `charge_box_site`
    ADD COLUMN `authorize_all` TINYINT(1) AFTER `remote_start`;    