ALTER TABLE `charge_box`
    ADD COLUMN `ip_address` VARCHAR(255) AFTER `endpoint_address`;

ALTER TABLE `charge_box`
    ADD COLUMN `remote_start` TINYINT(1) AFTER `ip_address`;

CREATE TABLE `charge_box_site` (
  `charge_box_id` varchar(255) NOT NULL,
  `site_id` int(11) NOT NULL,
  `tag_group_id` int(11),
  `customer_Id` int(11),
  `site_type_id` int(11),  
  `created_At` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_At` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`charge_box_id`)
) ENGINE=InnoDB CHARSET=latin1;


CREATE TABLE `ocpp_tag_detail` (
  `ocpp_tag_id` varchar(255) NOT NULL,
  `tag_group_id` int(11) NOT NULL,
  `customer_Id` int(11),
  `allow_concurrent_tx` TINYINT(1),
  `allow_public_charging` TINYINT(1),
  `is_active` TINYINT(1),
  `label` varchar(16),
  `created_At` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_At` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`ocpp_tag_id`)
) ENGINE=InnoDB CHARSET=latin1;


CREATE TABLE `connector_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connector_Type` varchar(16)  NOT NULL,
  `short_Description` varchar(32) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `power_Type` varchar(2) DEFAULT NULL,
  `max_power` decimal(5,2) unsigned,
  `updated_At` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_At` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=latin1; 

CREATE TABLE `connectors_detail` (
  `charge_box_id` varchar(255) NOT NULL,
  `connector_Id` int(11) NOT NULL,
  `connector_type_id` int(11) unsigned NOT NULL,
  `label` varchar(16),
  `barcode` varchar(16),
  `updated_At` datetime DEFAULT NULL,
  `created_At` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`charge_box_id`,`connector_Id`)
) ENGINE=InnoDB CHARSET=latin1;